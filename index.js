const express = require("express");
const app = express();
const port = 3000;

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const cors = require("cors")({
  origin: true
});

app.get("/", (req, res) => {
  res.send("Hello World!");
});

//GET query
app.get("/query", (req, res) => {
  cors(req, res, () => {
    var query_name = decodeURI(req.query.name);
    res.send(query_name);
  });
});

//POST body
app.post("/test", (req, res) => {
  cors(req, res, () => {
    var salesData = {
      first_item: req.body.first,
      second_item: req.body.second
    };
    res.send(salesData);
  });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
